# Dita Publishing Pipeline

The pipeline publishes the two Dita concept files in this repository, running
the conversion of each in parallel on an AWS r4.4xlarge spot instance acting as
a GitLab runner using the information in file .gitlab-ci.yml to determine the
actual processing.

The results can be seen at:

[concept 1](https://philiprbrenantest.gitlab.io/dita-publishing-pipeline/concept1.html)

and

[concept 2](https://philiprbrenantest.gitlab.io/dita-publishing-pipeline/concept2.html)

